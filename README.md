# piradio OFDM design repository



## Getting started

To create the Vivado project for the OFDM design :

- Open Vivado
- Create new project for the ZCU102 board
- From the settings, add ip_repo to the IP repositories
- run "source /path-to-tcl/ofdm_design.tcl"


